# Smart City Client

![](https://imgur.com/1XXKHSz.png)
![](https://imgur.com/5pROhfo.png)
![](https://imgur.com/qOsVhUp.png)


The users can look through all learning contents, vote for scenarios and watch the live dashboard. 


## Communication 

Communication between Angular and Unity is done via a websocket.

## Installing Angular 

In order to install Angular, you need to run npm install `-g @angular/cli`

## NG Charts Annotation

One also need to run `npm install chartjs-plugin-annotation --save` to install the annotation plugin for chartjs

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
