import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chapter1',
  templateUrl: './chapter1.component.html',
  styleUrls: ['./chapter1.component.css']
})

//Contains all contents of Chapter 1: 'Leben in der Stadt der Zukunft'

export class Chapter1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
