import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chapter3',
  templateUrl: './chapter3.component.html',
  styleUrls: ['./chapter3.component.css']
})

//Contains all contents of Chapter 3: 'Smart Mobility'

export class Chapter3Component implements OnInit {

  constructor() { }

  quiz1() {
    document.getElementById('1').setAttribute("style", "color:#629149;");
    document.getElementById('2').setAttribute("style", "color:#629149;");
    document.getElementById('3').setAttribute("style", "color:#629149;");
    document.getElementById('4').setAttribute("style", "color:#629149;");
    document.getElementById('5').setAttribute("style", "color:#629149;");
    document.getElementById('6').setAttribute("style", "color:#ff7c5e;");
    document.getElementById('7').setAttribute("style", "color:#ff7c5e;");
  }

  quiz2() {
    document.getElementById('true').setAttribute("style", "background-color:#629149;");
    document.getElementById('false').setAttribute("style", "background-color:#ff7c5e;");
  }

  quiz3() {
    document.getElementById('true1').setAttribute("style", "background-color:#629149;");
    document.getElementById('false1').setAttribute("style", "background-color:#ff7c5e;");
  }
  quiz4() {
    document.getElementById('true2').setAttribute("style", "background-color:#629149;");
    document.getElementById('false2').setAttribute("style", "background-color:#ff7c5e;");
  }
  quiz5() {
    document.getElementById('true3').setAttribute("style", "background-color:#629149;");
    document.getElementById('false3').setAttribute("style", "background-color:#ff7c5e;");
  }

  ngOnInit() {
  }

}
