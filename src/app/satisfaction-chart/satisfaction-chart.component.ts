import { Component, OnInit } from '@angular/core';
import { ChartType,ChartDataSets, ChartOptions } from 'chart.js';
import { MultiDataSet, Label, Color } from 'ng2-charts';
import { DashboardDataService } from '../dashboard-data.service';

@Component({
  selector: 'app-satisfaction-chart',
  templateUrl: './satisfaction-chart.component.html',
  styleUrls: ['./satisfaction-chart.component.css']
})

//Gets satisfaction data from dashboardDataService and displays it

export class SatisfactionChartComponent implements OnInit {

    // Doughnut
    public barChartOptions: ChartOptions = {
      legend: {
        labels: {
          fontColor: "white"
        }
      },
      responsive: true,
    };
    public doughnutChartLabels: Label[] = ['Zufriedenheit der Bevölkerung in %',];
    public barChartData: ChartDataSets[] = [
      { data: [0,0] },
    ];
    public doughnutChartType: ChartType = 'doughnut';
    chartColors: Color[] = [
      {
        borderColor: '#212121',
        backgroundColor: [
                    "rgb(253, 215, 54)",
                    "#212121",
               ],
      },
    ];
  
    constructor(private dashboardDataService: DashboardDataService) { }
  
    ngOnInit() {
      this.dashboardDataService.change.subscribe(data =>{
        let x = this.dashboardDataService.getSatisfaction()[0];
        let l = [];
        l.push(x);
        l.push(100-x);
        this.barChartData[0].data = l;
      })
    }
  }
