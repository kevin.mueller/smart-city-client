import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

//Displays all charts

export class DashboardComponent implements OnInit {

  mobile = false;
  desktop = true;

  constructor(
  ) { }

  ngOnInit() {
    if (window.screen.width < 500) {
      this.mobile = true;
      this.desktop = false;
    }
  }

}
