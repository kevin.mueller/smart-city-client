import { Component, OnInit, OnDestroy, AfterViewInit, OnChanges, DoCheck, AfterContentChecked, AfterViewChecked } from '@angular/core';
import { ScenariosService } from '../scenarios.service';
import { WebSocketService } from 'src/app/web-socket.service';
import { VoteService } from 'src/app/vote.service';

@Component({
  selector: 'app-scenario-list',
  templateUrl: './scenario-list.component.html',
  styleUrls: ['./scenario-list.component.css']
})

//Gets scenarios from scenariosService, voteService holds information if user is allowed to vote for specific scenario

export class ScenarioListComponent implements OnInit, AfterViewChecked {
  scenarios;
  votes = [true,true,true,true,true,true,true];

  constructor(private webSocketService: WebSocketService, private scenariosService: ScenariosService, private voteService: VoteService) {
    this.webSocketService.send("new");
    this.votes = voteService.getVotes();
   }

   //Executed when clicked on heart below scenario
  like(event,id){
    if (this.votes[id]) {
      this.webSocketService.send(id);
      this.votes[id] = false;
      this.voteService.setVotes(this.votes);
    }
  }

  ngAfterViewChecked(){
    this.disable();
  }

  //If voted, disable the button
  disable(){
    for(let i = 0; i < this.votes.length; i++){
      if(!this.votes[i]){
        let element = <HTMLInputElement> document.getElementById(i.toString());
        element.disabled = true;
      }
    }
  }

  ngOnInit() {
    this.votes = this.voteService.getVotes();
    this.scenarios=this.scenariosService.getScenarios();
    this.scenariosService.change.subscribe(data =>{
      this.scenarios = this.scenariosService.getScenarios().scenarios;
    })
  }

}
