import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chapter4',
  templateUrl: './chapter4.component.html',
  styleUrls: ['./chapter4.component.css']
})

//Contains all contents of Chapter 4: 'Smart Environment'

export class Chapter4Component implements OnInit {

  constructor() { }

  quiz() {
    document.getElementById('true').setAttribute("style", "background-color:#629149;");
    document.getElementById('false').setAttribute("style", "background-color:#ff7c5e;");
  }

  ngOnInit() {
  }

}
