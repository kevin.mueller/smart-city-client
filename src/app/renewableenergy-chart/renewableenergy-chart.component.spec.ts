import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewableenergyChartComponent } from './renewableenergy-chart.component';

describe('RenewableenergyChartComponent', () => {
  let component: RenewableenergyChartComponent;
  let fixture: ComponentFixture<RenewableenergyChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenewableenergyChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenewableenergyChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
