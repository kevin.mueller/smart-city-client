import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label, Color } from 'ng2-charts';
import { DashboardDataService } from '../dashboard-data.service';
import * as ChartAnnotation from 'chartjs-plugin-annotation';

@Component({
  selector: 'app-renewableenergy-chart',
  templateUrl: './renewableenergy-chart.component.html',
  styleUrls: ['./renewableenergy-chart.component.css']
})
export class RenewableenergyChartComponent implements OnInit {

  barChartOptions = {
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        value: 20,
        borderColor: 'rgba(255, 0, 0,0.9)',
        borderWidth: 4,
        label: {
          enabled: true,
          content: '',
          position: 'right'
        }
      }]
    },
    legend: {
      labels: {
        fontColor: "white"
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          suggestedMin: 0,
          suggestedMax: 100,
          fontColor: "rgba(255, 255, 255, 0.35)"
        },
        gridLines: {
          color: "rgba(255, 255, 255, 0.35)"
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: "rgba(255, 255, 255, 0.35)"
        },
        gridLines: {
          color: "rgba(255, 255, 255, 0.35)"
        }
      }],
    },
    responsive: true,
    plugins: [ChartAnnotation]
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [ChartAnnotation];
  chartColors: Color[] = [
    {
      borderColor: 'rgb(187, 134, 252)',
      backgroundColor: "rgba(187, 134, 252,0.75)",
    },
  ];

  public barChartData: ChartDataSets[] = [
    { data: [], label: 'Anteil der erneuerbaren Energien in %' },
  ];

  constructor(private dashboardDataService: DashboardDataService,) { }

  ngOnInit() {
    this.dashboardDataService.change.subscribe(data =>{
      this.barChartData[0].data = this.dashboardDataService.getRenewableenergy();
      for (let i = this.barChartLabels.length; i < this.barChartData[0].data.length; i++) {
          this.barChartLabels.push(('').toString());
        }
    })
  }

}

