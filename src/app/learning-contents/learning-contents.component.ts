import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-learning-contents',
  templateUrl: './learning-contents.component.html',
  styleUrls: ['./learning-contents.component.css']
})

//Displays all chapters

export class LearningContentsComponent implements OnInit {

  mobile = false;
  desktop = true;

  constructor() { }

  ngOnInit() {
    if (window.screen.width < 500) {
      this.mobile = true;
      this.desktop = false;
    }
  }

}
