import { Injectable , Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

//Holds scenarios, updates scenario list if scenarios change

export class ScenariosService {
  private scenarios;

  @Output() change: EventEmitter<any> = new EventEmitter();

  constructor() { }

  newData(data){
    this.scenarios = data;
    this.change.emit();
  }

  getScenarios(){
    return this.scenarios;
  }
}
