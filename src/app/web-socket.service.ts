import { Injectable, Output, EventEmitter } from '@angular/core';
import { webSocket } from 'rxjs/webSocket';

import { ScenariosService } from 'src/app/scenarios.service';
import { DashboardDataService } from 'src/app/dashboard-data.service';

@Injectable({
  providedIn: 'root'
})

//Gets data in form of json from angular, distribute it depending on message

export class WebSocketService {
  data = [];
  @Output() change: EventEmitter<any> = new EventEmitter();
  @Output() request: EventEmitter<any> = new EventEmitter();
  ws;
  url;

  constructor(
    private scenariosService: ScenariosService,
    private dashboardDataService: DashboardDataService,
    ) { 
      this.init('ws://localhost:6321/public');
  }

  close(){
    this.ws.unsubscribe();
  }

  onMessage(){
    this.ws.subscribe(
      (msg) => this.printer(msg),
      (err) => this.retry(err),
      () => this.retry('Disconnected')
    );
  }

  init(url){
    this.url = url;
    this.ws = webSocket(url);
    this.onMessage();
  }

  send(msg){
    msg = msg.toString();
    this.ws.next(msg);
  }

  retry(err){
    this.sleep(5000);
    this.init(this.url);
  }

  
  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  printer(msg){
    'co2' in msg ? this.dashboardDataService.newData(msg) : this.scenariosService.newData(msg)
  }


}
