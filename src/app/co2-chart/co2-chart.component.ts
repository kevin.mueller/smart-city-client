import { Component, OnInit } from '@angular/core';
import { ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { DashboardDataService } from '../dashboard-data.service';
import * as ChartAnnotation from 'chartjs-plugin-annotation';


@Component({
  selector: 'app-co2-chart',
  templateUrl: './co2-chart.component.html',
  styleUrls: ['./co2-chart.component.css']
})

//Gets air data from dashboardDataService and displays it

export class Co2ChartComponent implements OnInit {

  constructor(
    private dashboardDataService: DashboardDataService,
  ) { }

  ngOnInit() {
    this.dashboardDataService.change.subscribe(data =>{
      this.chartData[0].data = this.dashboardDataService.getCo2();
      for (let i = this.chartLabels.length; i < this.chartData[0].data.length; i++) {
          this.chartLabels.push('');
        }
    })
  }

  // CO2 Chart: 
  chartLegend = true;
  chartPlugins = [ChartAnnotation];
  chartType = 'line';
  chartData: ChartDataSets[] = [
    { data: [8], label: 'CO2 Emissionen' },
  ];
  chartLabels: Label[] = [];
  chartOptions = {
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        value: 80,
        borderColor: 'rgba(255, 0, 0,0.9)',
        borderWidth: 4,
        label: {
          enabled: true,
          content: '',
          position: 'right'
        }
      }]
    },
    legend: {
      labels: {
        fontColor: "white"
      }
    },
    scales: {
      yAxes: [{
        ticks:{
          suggestedMin: 0,
          suggestedMax: 100,
          fontColor: "rgba(255, 255, 255, 0.35)"
        },
        gridLines:{
          color:"rgba(255, 255, 255, 0.35)"
        }
      }],
      xAxes: [{
        ticks:{
          fontColor: "rgba(255, 255, 255, 0.35)"
        },
        gridLines:{
          color:"rgba(255, 255, 255, 0.35)"
        }
      }],
    },
    responsive: true,
  };
  chartColors: Color[] = [
    {
      borderColor: 'rgb(54, 162, 235)',
      backgroundColor: 'rgba(54, 162, 235,0.2)',
    },
  ];

}
