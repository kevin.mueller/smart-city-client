import { Component, OnInit } from '@angular/core';
import { element } from 'protractor';

@Component({
  selector: 'app-chapter2',
  templateUrl: './chapter2.component.html',
  styleUrls: ['./chapter2.component.css']
})
//Contains all contents of Chapter 2: 'Smartes Wohnen'

export class Chapter2Component implements OnInit {

  constructor() { }

  quiz(){
      document.getElementById('true').setAttribute("style","background-color:#629149;");
      document.getElementById('false').setAttribute("style","background-color:#ff7c5e;");
  }

  ngOnInit() {
  }

}
