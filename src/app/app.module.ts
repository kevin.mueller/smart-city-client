import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material';
import { MatSliderModule } from '@angular/material/slider';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule, MatSidenavModule, MatListModule } from  '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCardModule } from '@angular/material/card';
import { MatGridTileText } from '@angular/material/grid-list';
import { ChartsModule } from 'ng2-charts';
import { WebSocketService } from 'src/app/web-socket.service';
import { VoteService } from 'src/app/vote.service';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ScenarioListComponent } from './scenario-list/scenario-list.component';
import { StartComponent } from './start/start.component';
import { HelpComponent } from './help/help.component';
import { LearningContentsComponent } from './learning-contents/learning-contents.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavComponent } from './nav/nav.component';
import { Co2ChartComponent } from './co2-chart/co2-chart.component';
import { AirChartComponent } from './air-chart/air-chart.component';
import { SatisfactionChartComponent } from './satisfaction-chart/satisfaction-chart.component';
import { Chapter1Component } from './chapter1/chapter1.component';
import { Chapter2Component } from './chapter2/chapter2.component';
import { Chapter3Component } from './chapter3/chapter3.component';
import { Chapter4Component } from './chapter4/chapter4.component';
import { RenewableenergyChartComponent } from './renewableenergy-chart/renewableenergy-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    ScenarioListComponent,
    StartComponent,
    HelpComponent,
    LearningContentsComponent,
    DashboardComponent,
    NavComponent,
    Co2ChartComponent,
    AirChartComponent,
    SatisfactionChartComponent,
    Chapter1Component,
    Chapter2Component,
    Chapter3Component,
    Chapter4Component,
    RenewableenergyChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatButtonModule,
    MatToolbarModule,
    MatDividerModule,
    MatIconModule, 
    MatSidenavModule, 
    MatListModule,
    MatGridListModule,
    MatTooltipModule,
    MatCardModule,
    ChartsModule,
    RouterModule.forRoot([
      { path: 'scenarios', component: ScenarioListComponent },
      { path: 'learning-contents', component: LearningContentsComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'help', component: HelpComponent },
      { path: 'chapter-1', component: Chapter1Component },
      { path: 'chapter-2', component: Chapter2Component },
      { path: 'chapter-3', component: Chapter3Component },
      { path: 'chapter-4', component: Chapter4Component },
      { path: '', component: StartComponent },
    ])
  ],
  providers: [WebSocketService, VoteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
