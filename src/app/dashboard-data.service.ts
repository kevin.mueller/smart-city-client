import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

//Holds all data and EventEmitter for charts

export class DashboardDataService {
  co2 = [];
  air = [];
  renewableenergy = [];
  satisfaction = [];

  @Output() change: EventEmitter<any> = new EventEmitter();

  constructor() { }

  newData(data){
    this.co2 = data.co2;
    this.air = data.air;
    this.renewableenergy = data.renewableenergy;
    this.satisfaction = data.satisfaction;
    this.change.emit();
  }

  getAir(){
    return this.air;
  }

  getCo2(){
    return this.co2;
  }

  getRenewableenergy(){
    return this.renewableenergy;
  }

  getSatisfaction(){
    return this.satisfaction;
  }
}
