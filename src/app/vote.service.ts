import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

//Holds user votes

export class VoteService {

  votes = [true, true, true, true, true, true, true];

  constructor() { }

  getVotes(){
    return this.votes;
  }

  setVotes(arr){
    this.votes = arr;
  }


}
